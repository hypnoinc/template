
window.Lockr = {
	get: function(key){

	    try {
	      	var value = JSON.parse(localStorage.getItem(key));
	    } catch (e) {
            if(localStorage[key]) {
              	value = {data: localStorage.getItem(key)};
            } else{
                value = null;
            }
	    }
	    if(!value) {
	      	return false;
	    } else if (typeof value === 'object' && typeof value.data !== 'undefined') {
	      	return value.data;
	    }
	    

	},
    
    set: function(key, value){
	    try {
	      localStorage.setItem(key, JSON.stringify({"data": value}));
	    } catch (e) {
	      if (console) console.warn("Lockr didn't successfully save the '{"+ key +": "+ value +"}' pair, because the localStorage is full.");
	    }
    }

};